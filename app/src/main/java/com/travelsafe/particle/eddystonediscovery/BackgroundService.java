package com.travelsafe.particle.eddystonediscovery;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class BackgroundService extends Service {
    private static final String TAG = "main2";
    // An aggressive scan for nearby devices that reports immediately.
    private static final ScanSettings SCAN_SETTINGS =
            new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).setReportDelay(0)
                    .build();
    // The Eddystone Service UUID, 0xFEAA.
    private static final ParcelUuid EDDYSTONE_SERVICE_UUID =
            ParcelUuid.fromString("0000FEAA-0000-1000-8000-00805F9B34FB");
    private static ArrayList<Integer> decisionArray = new ArrayList<>();
    String finalBeacon;
    int finalRSSI;
    int rssiThreshHold;
    int criticalSizeOfDecisionArray;
    int scanPeriod;
    NotificationCompat.Builder mBuilder;
    private BluetoothLeScanner scanner;
    private SharedPreferences sharedPreferences;

    public BackgroundService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        init();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        scanPeriod = sharedPreferences.getInt(SettingsActivity.SCAN_PERIOD, 5) * 1000;
        rssiThreshHold = sharedPreferences.getInt(SettingsActivity.RSSI_THRESHOLD, 20);
        criticalSizeOfDecisionArray = sharedPreferences.getInt(SettingsActivity.CRITICAL_SIZE_OF_DECISION_ARAAY, 5);

        Log.v(TAG, "Background Scanning begins");
        finalBeacon = intent.getStringExtra(Constants.MONITOR_BEACON_NAME);
        finalRSSI = intent.getIntExtra(Constants.MONITOR_BEACON_RSSI, 0);

        if (finalRSSI == 0 || finalBeacon.equals("")) {
            Toast.makeText(getApplicationContext(), "No Value Selected", Toast.LENGTH_LONG).show();
            onDestroy();
        }
        makeToast("Target Beacon: "+finalBeacon + " // f-RSSI: " + String.valueOf(finalRSSI));

        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setAutoCancel(true);
        mBuilder.setSmallIcon(R.drawable.ic_change_history_black_24dp);
        mBuilder.setTicker("Background Scanning has been started");
        mBuilder.setWhen(System.currentTimeMillis());
        mBuilder.setContentTitle("Travel Safe");
        mBuilder.setContentText("Target is secure, scanning is going on ...");
        Intent nIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, nIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);
        final NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(112314, mBuilder.build());

        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            @Override
            public void run() {
                    scanAndReport();
                    scanPeriod = sharedPreferences.getInt(SettingsActivity.SCAN_PERIOD, 5) * 1000;
                    handler.postDelayed(this, scanPeriod);
            }
        };
            r.run();
        return START_STICKY;

    }


    public void makeToast(String msg) {
        Toast.makeText(getApplicationContext(), "InS " + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        //super.onDestroy();
        Intent stopService = new Intent(getApplicationContext(), BackgroundService.class);
        getApplicationContext().stopService(stopService);
    }

    public void scanAndReport() {
        rssiThreshHold = sharedPreferences.getInt(SettingsActivity.RSSI_THRESHOLD, 20);
        criticalSizeOfDecisionArray = sharedPreferences.getInt(SettingsActivity.CRITICAL_SIZE_OF_DECISION_ARAAY, 5);

        List<ScanFilter> scanFilters = new ArrayList<>();
        scanFilters.add(new ScanFilter.Builder().setServiceUuid(EDDYSTONE_SERVICE_UUID).build());
        if (scanner != null) {
            scanner.startScan(scanFilters, SCAN_SETTINGS, new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    Log.d(TAG, "OnScanResult");
                    String deviceAddress = result.getDevice().getAddress();
                    int currentRSSI = result.getRssi();
                    int difference = -currentRSSI - (-finalRSSI);
                    Log.d(TAG, "C-RSSI: " + String.valueOf(currentRSSI) + "// f-RSSI: " + String.valueOf(finalRSSI) + "// C-Limit: " + String.valueOf(decisionArray.size()));
                    Log.d(TAG, "Difference is " + String.valueOf(difference) + " // Threshold Limit: " + String.valueOf(rssiThreshHold));
                    if (finalBeacon != null) {
                        if (deviceAddress.equals(finalBeacon)) {
                            if (difference > rssiThreshHold) {
                                decisionArray.add(currentRSSI);
                                Log.d(TAG, "Array increased, size is: " + String.valueOf(decisionArray.size()));
                                int s = decisionArray.size();
                                if (s == criticalSizeOfDecisionArray) {
                                    Log.d(TAG, "array size critical reached, Launching activity");
                                    Intent alertIntent = new Intent(getApplicationContext(), FullScreenAlertActivity.class);
                                    alertIntent.putExtra(Constants.MONITOR_BEACON_NAME, finalBeacon);
                                    alertIntent.putExtra(Constants.MONITOR_BEACON_RSSI, finalRSSI);
                                    startActivity(alertIntent);
                                    finalBeacon = " ";
                                    decisionArray.clear();
                                }
                            } else {
                                decisionArray.clear();
                            }
                        }
                    }
                }
                @Override
                public void onScanFailed(int errorCode) {
                    switch (errorCode) {
                        case SCAN_FAILED_ALREADY_STARTED:
                            //logErrorAndShowToast("SCAN_FAILED_ALREADY_STARTED");
                            break;
                        case SCAN_FAILED_APPLICATION_REGISTRATION_FAILED:
                            //init(true);
                            //onResume();
                            //logErrorAndShowToast("SCAN_FAILED_APPLICATION_REGISTRATION_FAILED");
                            break;
                        case SCAN_FAILED_FEATURE_UNSUPPORTED:
                            //logErrorAndShowToast("SCAN_FAILED_FEATURE_UNSUPPORTED");
                            break;
                        case SCAN_FAILED_INTERNAL_ERROR:
                            //logErrorAndShowToast("SCAN_FAILED_INTERNAL_ERROR");
                            break;
                        default:
                            //logErrorAndShowToast("Scan failed, unknown error code");
                            break;
                    }
                }
            });
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented ...");
    }

    // Attempts to create the scanner.
    private void init() {
        try {
            BluetoothManager manager = (BluetoothManager) getApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE);
            BluetoothAdapter btAdapter = manager.getAdapter();
            scanner = btAdapter.getBluetoothLeScanner();
        }catch (Exception e){
            makeToast(e.toString());
        }
    }
}
