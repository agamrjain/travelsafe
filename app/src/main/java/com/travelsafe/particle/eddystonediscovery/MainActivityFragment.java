
package com.travelsafe.particle.eddystonediscovery;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelUuid;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class MainActivityFragment extends Fragment {

    private static final String TAG = "main1";
    private static final int REQUEST_ENABLE_BLUETOOTH = 1;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 2;
    private static final String STATUS_MONITOR = "Monitoring";
    private static final String STATUS_NOT_MONITORING = "Not Monitoring";
    String finalBeacon;
    int finalRSSI = 0;
    private static final ScanSettings SCAN_SETTINGS =
            new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).setReportDelay(0)
                    .build();

    private static final Handler handler = new Handler(Looper.getMainLooper());

    // The Eddystone Service UUID, 0xFEAA.
    private static final ParcelUuid EDDYSTONE_SERVICE_UUID =
            ParcelUuid.fromString("0000FEAA-0000-1000-8000-00805F9B34FB");

    private BluetoothLeScanner scanner;
    private BeaconArrayAdapter arrayAdapter;

    private List<ScanFilter> scanFilters;
    private ScanCallback scanCallback;

    private Map<String, Beacon> deviceToBeaconMap = new HashMap<>();

    private EditText filter;

    private SharedPreferences sharedPreferences;
    private int onLostTimeoutMillis;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"OnCreate");
        init(false);

        ArrayList<Beacon> arrayList = new ArrayList<>();
        arrayAdapter = new BeaconArrayAdapter(getActivity(), R.layout.beacon_list_item, arrayList);

        Intent receivedIntent = getActivity().getIntent();
        if (receivedIntent.getStringExtra(Constants.BUTTON_PRESSED)!=null)
            {
                finalBeacon = getActivity().getIntent().getStringExtra(Constants.MONITOR_BEACON_NAME);
                finalRSSI = getActivity().getIntent().getIntExtra(Constants.MONITOR_BEACON_RSSI, 0);
                Beacon beacon = new Beacon(finalBeacon, finalRSSI, STATUS_MONITOR);
                arrayAdapter.clear();
                arrayAdapter.add(beacon);
                SetBeaconForMonitoring(beacon);
        }
        scanFilters = new ArrayList<>();
        scanFilters.add(new ScanFilter.Builder().setServiceUuid(EDDYSTONE_SERVICE_UUID).build());
        scanCallback = new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                ScanRecord scanRecord = result.getScanRecord();
                if (scanRecord == null) {
                    return;
                }
                if (finalBeacon != null) {
                    Log.d("rssi", " fixed rssi is: " + finalRSSI);
                }
                String deviceAddress = result.getDevice().getAddress();
                Beacon beacon;
                if (!deviceToBeaconMap.containsKey(deviceAddress)) {
                    beacon = new Beacon(deviceAddress, result.getRssi(), STATUS_NOT_MONITORING);
                    deviceToBeaconMap.put(deviceAddress, beacon);
                    arrayAdapter.add(beacon);
                } else {
                    deviceToBeaconMap.get(deviceAddress).lastSeenTimestamp = System.currentTimeMillis();
                    deviceToBeaconMap.get(deviceAddress).rssi = result.getRssi();
                    Log.d("MainActivity", "Old Device Found with Address: " +deviceAddress+" RSSI: "+String.valueOf(result.getRssi()));
                }
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onScanFailed(int errorCode) {
                switch (errorCode) {
                    case SCAN_FAILED_ALREADY_STARTED:
                        logErrorAndShowToast("SCAN_FAILED_ALREADY_STARTED");
                        break;
                    case SCAN_FAILED_APPLICATION_REGISTRATION_FAILED:
                         init(true);
                         onResume();
                        logErrorAndShowToast("SCAN_FAILED_APPLICATION_REGISTRATION_FAILED");
                        break;
                    case SCAN_FAILED_FEATURE_UNSUPPORTED:
                        logErrorAndShowToast("SCAN_FAILED_FEATURE_UNSUPPORTED");
                        break;
                    case SCAN_FAILED_INTERNAL_ERROR:
                        logErrorAndShowToast("SCAN_FAILED_INTERNAL_ERROR");
                        break;
                    default:
                        logErrorAndShowToast("Scan failed, unknown error code");
                        break;
                }
            }
        };
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        Button testButton = (Button) view.findViewById(R.id.testService);
        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), BackgroundService.class);
                getActivity().stopService(intent);

            }
        });
        filter = (EditText) view.findViewById(R.id.filter);
        filter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // NOP
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // NOP
            }

            @Override
            public void afterTextChanged(Editable s) {
                arrayAdapter.getFilter().filter(filter.getText().toString());
            }
        });
        final ListView listView = (ListView) view.findViewById(R.id.listView);
        listView.setAdapter(arrayAdapter);
        listView.setEmptyView(view.findViewById(R.id.placeholder));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Beacon beacon = (Beacon) listView.getItemAtPosition(position);
                SetBeaconForMonitoring(beacon);
            }
        });
        return view;

    }


    public void makeToast(String msg){
        Toast.makeText(getActivity(),msg,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.v(TAG, "OnPause");
        if (scanner != null) {
            scanner.stopScan(scanCallback);
        }

        /*Intent serviceIntent = new Intent(getContext(),BackgroundService.class);
        serviceIntent.putExtra("finalBeacon", finalBeacon);
        serviceIntent.putExtra("finalRSSI", finalRSSI);
        getActivity().startService(serviceIntent);*/

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"OnResume");
        handler.removeCallbacksAndMessages(null);

        int timeoutMillis = 5000;

        if (timeoutMillis > 0) {  // 0 is special and means don't remove anything.
            onLostTimeoutMillis = timeoutMillis;
            setOnLostRunnable();
        }

        if (false) {
            Runnable updateTitleWithNumberSightedBeacons = new Runnable() {
                final String appName = getActivity().getString(R.string.app_name);

                @Override
                public void run() {
                    getActivity().setTitle(appName + " (" + deviceToBeaconMap.size() + ")");
                    handler.postDelayed(this, 1000);
                }
            };
            handler.postDelayed(updateTitleWithNumberSightedBeacons, 1000);
        } else {
            getActivity().setTitle(getActivity().getString(R.string.app_name));
        }

        if (scanner != null) {
            scanner.startScan(scanFilters, SCAN_SETTINGS, scanCallback);
        }
    }

    private void setOnLostRunnable() {
        Runnable removeLostDevices = new Runnable() {
            @Override
            public void run() {
                long time = System.currentTimeMillis();
                Iterator<Entry<String, Beacon>> itr = deviceToBeaconMap.entrySet().iterator();
                while (itr.hasNext()) {
                    Beacon beacon = itr.next().getValue();
                    if ((time - beacon.lastSeenTimestamp) > onLostTimeoutMillis) {
                        itr.remove();
                        arrayAdapter.remove(beacon);
                    }
                }
                handler.postDelayed(this, onLostTimeoutMillis);
            }
        };
        handler.postDelayed(removeLostDevices, onLostTimeoutMillis);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_OK) {
                init(false);
            } else {
                getActivity().finish();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "PERMISSION_REQUEST_COARSE_LOCATION granted");
                } else {
                    showFinishingAlertDialog("Coarse location access is required",
                            "App will close since the permission was denied");
                }
            }
        }
    }

    // Attempts to create the scanner.
    private void init(Boolean error) {
        // New Android M+ permission check requirement.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("This app needs coarse location access");
                builder.setMessage("Please grant coarse location access so this app can scan for beacons");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                                PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });
                builder.show();
            }
        }

        BluetoothManager manager = (BluetoothManager) getActivity().getApplicationContext()
                .getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter btAdapter = manager.getAdapter();
        if (error){
            btAdapter.disable();
            /*Intent disableBtIntent = new Intent(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED);
            this.startActivityForResult(disableBtIntent, REQUEST_D);*/
        }

        if (btAdapter == null) {
            showFinishingAlertDialog("Bluetooth Error", "Bluetooth not detected on device");
        } else if (!btAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            this.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BLUETOOTH);
        } else {
            scanner = btAdapter.getBluetoothLeScanner();
        }
    }


    public void SetBeaconForMonitoring(Beacon beacon) {
        finalBeacon = beacon.deviceAddress;
        finalRSSI = beacon.rssi;
        beacon.status = STATUS_MONITOR;
        try {
            Intent serviceIntent = new Intent(getContext(),BackgroundService.class);
            serviceIntent.putExtra(Constants.MONITOR_BEACON_NAME, finalBeacon);
            serviceIntent.putExtra(Constants.MONITOR_BEACON_RSSI, finalRSSI);
            getActivity().startService(serviceIntent);
            arrayAdapter.notifyDataSetChanged();
        }catch (Exception e){
            makeToast(e.toString());
        }
    }

    // Pops an AlertDialog that quits the app on OK.
    private void showFinishingAlertDialog(String title, String message) {
        new AlertDialog.Builder(getActivity()).setTitle(title).setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getActivity().finish();
                    }
                }).show();
    }

    private void logErrorAndShowToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        Log.e(TAG, message);
    }

}
