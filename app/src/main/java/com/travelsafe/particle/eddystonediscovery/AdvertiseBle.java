package com.travelsafe.particle.eddystonediscovery;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.os.ParcelUuid;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.nio.charset.Charset;
import java.util.UUID;

public class AdvertiseBle extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertise_ble);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setProgress(0);

        final TextView adstatus = (TextView) findViewById(R.id.adStatus);
        adstatus.setVisibility(View.INVISIBLE);
        try {
            final BluetoothLeAdvertiser advertiser = BluetoothAdapter.getDefaultAdapter().getBluetoothLeAdvertiser();
            AdvertiseSettings settings = new AdvertiseSettings.Builder()
                    .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY)
                    .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH)
                    .setConnectable(false)
                    .build();

            ParcelUuid pUuid = new ParcelUuid(UUID.fromString(getString(R.string.ble_uuid)));

            AdvertiseData data = new AdvertiseData.Builder()
                    .setIncludeDeviceName(true)
                    .addServiceUuid(pUuid)
                    .addServiceData(pUuid, "Data".getBytes(Charset.forName("UTF-8")))
                    .build();

            final AdvertiseCallback advertisingCallback = new AdvertiseCallback() {
                @Override
                public void onStartSuccess(AdvertiseSettings settingsInEffect) {
                    super.onStartSuccess(settingsInEffect);
                    progressBar.setProgress(100);
                    adstatus.setVisibility(View.VISIBLE);
                    adstatus.append("Adverting ... ");
                }

                @Override
                public void onStartFailure(int errorCode) {
                    Log.e("BLE", "Advertising onStartFailure: " + errorCode);
                    super.onStartFailure(errorCode);
                }

            };

            advertiser.startAdvertising(settings, data, advertisingCallback);
            Log.d("agam", "Advertise begins");

            ImageButton stopBleButton = (ImageButton) findViewById(R.id.imageButton);
            stopBleButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    advertiser.stopAdvertising(advertisingCallback);
                }
            });
        }catch (Exception e){
            Log.d("agam", e.toString());
        }
        }


}
