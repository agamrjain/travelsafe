package com.travelsafe.particle.eddystonediscovery;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static android.content.ContentValues.TAG;

public class StartMonitoring extends Activity {

    String finalBeacon;
    int finalRSSI;
    int rssiThreshHold;
    int criticalSizeOfDecisionArray;
    int scanPeriod;
    private SharedPreferences sharedPreferences;
    private final static String TAG = "MonitorActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_monitoring);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        scanPeriod = sharedPreferences.getInt(SettingsActivity.SCAN_PERIOD, 5) * 1000;
        rssiThreshHold = sharedPreferences.getInt(SettingsActivity.RSSI_THRESHOLD, 20);
        criticalSizeOfDecisionArray = sharedPreferences.getInt(SettingsActivity.CRITICAL_SIZE_OF_DECISION_ARAAY, 5);

        Log.v(TAG, "Monitor Activity begins");
        finalBeacon = intent.getStringExtra("finalBeacon");
        finalRSSI = intent.getIntExtra("finalRSSI", 0);

        TextView deviceAddress = (TextView) findViewById(R.id.deviceAddress2);
        TextView rssi = (TextView) findViewById(R.id.rssi2);
        TextView scanPeriod = (TextView) findViewById(R.id.scanPeriod2);
        TextView rssiTh = (TextView) findViewById(R.id.rssiThreshold2);
        TextView csda = (TextView) findViewById(R.id.csda2);

        deviceAddress.append(finalBeacon);
        rssi.append(String.valueOf(finalRSSI));
        scanPeriod.append(String.valueOf(scanPeriod));
        rssiTh.append(String.valueOf(rssiThreshHold));
        csda.append(String.valueOf(criticalSizeOfDecisionArray));


        Button resetButtion = (Button) findViewById(R.id.resetButton);
        resetButtion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resetIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(resetIntent);
            }
        });



    }

}
